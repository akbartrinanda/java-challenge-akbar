package ist.challenge.akbar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import ist.challenge.akbar.models.User;
import ist.challenge.akbar.repositories.UserRepository;
import ist.challenge.akbar.services.UserServiceImpl;

@SpringBootTest
class AkbarApplicationTests {
	@InjectMocks
	private UserServiceImpl userService;
	@Mock
	private UserRepository userRepository;

	@Test
	void findAllUsers() {
		Mockito.when(userRepository.findAll())
				.thenReturn(Arrays.asList(
					new User(1L, "Anton", "123"),
					new User(2L, "Dina", "234"),
					new User(3L, "Bima", "345")
				));

		List<User> users = userService.getAllUsers();

		// ASSERTION
		assertEquals("Anton", users.get(0).getUsername());
		assertEquals("Dina", users.get(1).getUsername());
		assertEquals("Bima", users.get(2).getUsername());
	}

}
