package ist.challenge.akbar.services;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ist.challenge.akbar.models.UserDto;
import ist.challenge.akbar.models.User;
import ist.challenge.akbar.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  UserRepository userRepository;

  @Override
  public ResponseEntity<?> register(UserDto userDto) {
    // check username is unique
    User user = userRepository.findByUsername(userDto.getUsername());
    if (user != null) {
      return ResponseEntity.status(HttpStatus.CONFLICT).body("Username sudah terpakai");
    }

    // save user
    user = new User();
    user.setUsername(userDto.getUsername());
    user.setPassword(userDto.getPassword());
    userRepository.save(user);

    return ResponseEntity.status(HttpStatus.CREATED).body("Sukses");
  }

  @Override
  public ResponseEntity<?> login(UserDto userDto) {
    // check data is not empty
    if (userDto.getUsername().isEmpty() || userDto.getPassword().isEmpty()) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username dan / atau password kosong");
    }

    // check username and password
    User user = userRepository.findByUsername(userDto.getUsername());
    if (user == null) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Username tidak cocok");
    } else if (!user.getPassword().equals(userDto.getPassword())) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Password tidak cocok");
    } else {
      return ResponseEntity.status(HttpStatus.OK).body("Sukses Login");
    }
  }

  @Override
  public List<User> getAllUsers() {
    // find all users
    return userRepository.findAll();
  }

  @Override
  public ResponseEntity<?> update(Long id, UserDto userDto) {
    // check data is not empty
    if (userDto.getUsername().isEmpty() || userDto.getPassword().isEmpty()) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username dan / atau password kosong");
    }

    // check id
    Optional<User> user = userRepository.findById(id);
    if (!user.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User tidak ditemukan");
    }

    // check username
    User userCheck = userRepository.findByUsername(userDto.getUsername());
    if (userCheck != null && !userCheck.getId().equals(id)) {
      return ResponseEntity.status(HttpStatus.CONFLICT).body("Username sudah terpakai");
    }

    // check password
    if (user.get().getPassword().equals(userDto.getPassword())) {
      return ResponseEntity.status(HttpStatus.CONFLICT).body("Password tidak boleh sama dengan password sebelumnya");
    }

    // update
    user.get().setUsername(userDto.getUsername());
    user.get().setPassword(userDto.getPassword());
    userRepository.save(user.get());

    return ResponseEntity.status(HttpStatus.CREATED).body("Sukses");

  }

}
