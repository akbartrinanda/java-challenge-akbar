package ist.challenge.akbar.services;

import java.util.List;
import org.springframework.http.ResponseEntity;
import ist.challenge.akbar.models.UserDto;
import ist.challenge.akbar.models.User;

public interface UserService {
  public ResponseEntity<?> register(UserDto userDto);

  public ResponseEntity<?> login(UserDto userDto);

  public List<User> getAllUsers();

  public ResponseEntity<?> update(Long id, UserDto userDto);
}
