package ist.challenge.akbar.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ist.challenge.akbar.models.UserDto;
import ist.challenge.akbar.models.User;
import ist.challenge.akbar.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
  @Autowired
  UserService userService;

  @PostMapping("/register")
  public ResponseEntity<?> register(@RequestBody UserDto userDto) {
    return userService.register(userDto);
  }

  @PostMapping("/login")
  public ResponseEntity<?> login(@RequestBody UserDto userDto) {
    return userService.login(userDto);
  }

  @GetMapping("")
  public List<User> getAllUsers() {
    return userService.getAllUsers();
  }

  @PutMapping("/update")
  public ResponseEntity<?> update(@RequestParam Long id, @RequestBody UserDto userDto) {
    return userService.update(id, userDto);
  }
}
